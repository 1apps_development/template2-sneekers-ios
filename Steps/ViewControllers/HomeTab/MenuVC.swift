//
//  MenuVC.swift
//  Sneaker
//
//  Created by DREAMWORLD on 15/04/22.
//

import UIKit
import SwiftyJSON
import SDWebImage

protocol ProductListDelegate {
    func getdata(subcategory_id:String,maincategory_id:String,categories_name:String)
}

class MenuCell : UITableViewCell
{
    @IBOutlet weak var img_Categories: UIImageView!
    @IBOutlet weak var lbl_title: UILabel!
}

class MenuVC: UIViewController {
    
    @IBOutlet weak var btn_login: UIButton!
    @IBOutlet weak var Tableview_MenuList: UITableView!
    var menuListArray = [JSON]()
    var sections = [Section]()
    var delegate: ProductListDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == ""
        {
            self.btn_login.isHidden = false
        }
        else{
            
            self.btn_login.isHidden = true
        }
        Tableview_MenuList.register(UINib(nibName: "ExpandableHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "ExpandableHeaderView")
        let urlString = API_URL + "navigation"
        let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
        let params: NSDictionary = ["theme_id":APP_THEME]
        self.Webservice_navigation(url: urlString, params: params, header: headers)
    }
}
//MARK: Button actions
extension MenuVC
{
    @IBAction func btnTap_youtube(_ sender: UIButton) {
        guard let url = URL(string: UserDefaultManager.getStringFromUserDefaults(key: UD_YoutubeURL)) else {
            return
        }
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    @IBAction func btnTap_msg(_ sender: UIButton) {
        guard let url = URL(string: UserDefaultManager.getStringFromUserDefaults(key: UD_MessageURL)) else {
            return
        }
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    @IBAction func btnTap_insta(_ sender: UIButton) {
        guard let url = URL(string: UserDefaultManager.getStringFromUserDefaults(key: UD_InstaURL)) else {
            return
        }
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    @IBAction func btnTap_twitter(_ sender: UIButton) {
        guard let url = URL(string: UserDefaultManager.getStringFromUserDefaults(key: UD_TwitterURL)) else {
            return
        }
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func btnTap_Close(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnTap_Login(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
        let nav : UINavigationController = UINavigationController(rootViewController: objVC)
        nav.navigationBar.isHidden = true
        keyWindow?.rootViewController = nav
    }
    
}
extension MenuVC: ExpandableHeaderViewDelegate {
    
    func toogleSection(header: ExpandableHeaderView, section: Int) {
        for theSection in 0..<sections.count {
            if theSection == section {
                sections[theSection].expanded = !sections[section].expanded
            } else {
                sections[theSection].expanded = false
            }
        }
        Tableview_MenuList.beginUpdates()
        for i in 0 ..< sections[section].subcategory.count {
            Tableview_MenuList.reloadRows(at: [IndexPath(row: i, section: section)], with: .automatic)
        }
        Tableview_MenuList.endUpdates()
    }
}
//MARK: Tableview Methods
extension MenuVC: UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.sections[section].subcategory.count)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let containerView = tableView.headerView(forSection: indexPath.section) as? ExpandableHeaderView else { return 0}
        if sections[indexPath.section].expanded {
            DispatchQueue.main.async {
                containerView.openArrow()
            }
            return UITableView.automaticDimension
        } else {
            DispatchQueue.main.async {
                containerView.closeArrow()
            }
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "ExpandableHeaderView") as! ExpandableHeaderView
        header.common()
        header.delegate = self
        header.section = section
        header.titlePayment.text = sections[section].category
        return header
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.Tableview_MenuList.dequeueReusableCell(withIdentifier: "MenuCell") as! MenuCell
        let data = self.sections[indexPath.section].subcategory[indexPath.row]
        cell.lbl_title.text = data["name"]!
        cell.img_Categories.sd_setImage(with: URL(string: IMG_URL + data["image"]!), placeholderImage: UIImage(named: ""))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = self.sections[indexPath.section].subcategory[indexPath.row]
        self.dismiss(animated: true) {
            self.delegate.getdata(subcategory_id: data["subcategory_id"]!, maincategory_id: data["maincategory_id"]!, categories_name: data["name"]!)
        }
    }
    
}

//MARK: Api Calling Function
extension MenuVC
{
    func Webservice_navigation(url:String, params:NSDictionary, header:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: header, parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ statusCode:String) in
            let status = jsonResponse!["status"].stringValue
            if status == "1"
            {
                let jsondata = jsonResponse!["data"].arrayValue
                var innersubcategory = [[String:String]]()
                for data in jsondata
                {
                    innersubcategory.removeAll()
                    for innercate in data["sub_category"].arrayValue
                    {
                        let obj = ["name":innercate["name"].stringValue,"image":innercate["icon_img_path"].stringValue,"subcategory_id":innercate["subcategory_id"].stringValue,"isSelected":"0","maincategory_id":innercate["maincategory_id"].stringValue]
                        innersubcategory.append(obj)
                    }
                    self.sections.append(contentsOf: [Section(category: data["name"].stringValue, subcategory: innersubcategory, expanded: false)])
                }
                
                self.Tableview_MenuList.delegate = self
                self.Tableview_MenuList.dataSource = self
                self.Tableview_MenuList.reloadData()
            }
            
            else if status == "9"
            {
                UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userId)
                UserDefaultManager.setStringToUserDefaults(value: "", key: UD_BearerToken)
                UserDefaultManager.setStringToUserDefaults(value: "", key: UD_TokenType)
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                nav.navigationBar.isHidden = true
                keyWindow?.rootViewController = nav
            }
            else
            {
                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: jsonResponse!["data"]["message"].stringValue.replacingOccurrences(of: "\\n", with: "\n"))
            }
        }
    }
}

