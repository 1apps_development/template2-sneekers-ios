import UIKit
import SwiftyJSON
import SDWebImage

class featuredListCell : UICollectionViewCell
{
    @IBOutlet weak var img_cat: UIImageView!
    @IBOutlet weak var cell_view: UIView!
    @IBOutlet weak var lbl_title: UILabel!
}

class featuredItemListCell : UICollectionViewCell
{
    @IBOutlet weak var btn_favrites: UIButton!
    @IBOutlet weak var lbl_itemdiscountPrice: UILabel!
    @IBOutlet weak var lbl_itemPrice: UILabel!
    @IBOutlet weak var lbl_itemname: UILabel!
    @IBOutlet weak var img_item: UIImageView!
    @IBOutlet weak var btn_cart: UIButton!
    @IBOutlet weak var lbl_currency: UILabel!
    @IBOutlet weak var lbl_tag: UILabel!
}

class HomeVC: UIViewController {
    
    // home-header
    @IBOutlet weak var home_header_title_text: UILabel!
    @IBOutlet weak var home_header_sub_text: UILabel!
    @IBOutlet weak var home_header_bg_image: UIImageView!
    
    // home-featured-product
    @IBOutlet weak var home_featured_product_text: UILabel!
    @IBOutlet weak var home_featured_product_title: UILabel!
    
    // home-categories
    @IBOutlet weak var home_categories_text: UILabel!
    @IBOutlet weak var home_categories_title: UILabel!
    @IBOutlet weak var home_categories_button: UIButton!
    
    // home-bestsellers
    @IBOutlet weak var home_bestsellers_text: UILabel!
    @IBOutlet weak var home_bestsellers_title: UILabel!
    @IBOutlet weak var home_bestsellers_button: UIButton!
    
    // home-loyalty-program
    @IBOutlet weak var home_loyalty_program_text: UILabel!
    @IBOutlet weak var home_loyalty_program_title: UILabel!
    @IBOutlet weak var home_loyalty_program_button: UIButton!
    @IBOutlet weak var home_loyalty_program_sub_text: UILabel!
    @IBOutlet weak var home_loyalty_program_bg_image: UIImageView!
    
    @IBOutlet weak var Height_Tableview: NSLayoutConstraint!
    @IBOutlet weak var Tableview_CategoriesList: UITableView!
    @IBOutlet weak var Collectionview_TrendingList: UICollectionView!
    @IBOutlet weak var Collectionview_FeaturedProductsList: UICollectionView!
    @IBOutlet weak var Collectionview_FeaturedList: UICollectionView!
    @IBOutlet weak var Collectionview_BestSellersList: UICollectionView!
    @IBOutlet weak var Height_ViewLoyalti: NSLayoutConstraint!
    @IBOutlet weak var View_Loyalti: UIView!
    @IBOutlet weak var lbl_count: UILabel!
    @IBOutlet weak var view_Empty: UIView!
    
    var Categories_Array = [JSON]()
    var Trending_Categories_Array = [JSON]()
    var main_category_id_trending = String()
    var Home_Categories_Array = [[String:String]]()
    var pageIndex = 1
    var lastIndex = 0
    var pageIndex_best = 1
    var lastIndex_best = 0
    
    var pageIndex_trending = 1
    var lastIndex_trending = 0
    
    var pageIndex_maincategory = 1
    var lastIndex_maincategory = 0
    
    var SelectedCategoryid = String()
    var SelectedSubCategoryid = String()
    
    var Featured_Products_Array = [[String:String]]()
    var Bestseller_Products_Array = [[String:String]]()
    var Trending_Products_Array = [[String:String]]()
    var selectedindex = 0
    var selectedindex_Trending = 0
    
    var product_id = String()
    var Selected_Variant_id = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view_Empty.isHidden = false
        cornerRadius(viewName: self.lbl_count, radius: self.lbl_count.frame.height / 2)
        self.tabBarController?.tabBar.isHidden = false
        if UserDefaults.standard.value(forKey: UD_GuestObj) != nil
        {
            let Guest_Array = UserDefaultManager.getCustomObjFromUserDefaultsGuest(key: UD_GuestObj) as! [[String:String]]
            UserDefaultManager.setStringToUserDefaults(value: "\(Guest_Array.count)", key: UD_CartCount)
            self.lbl_count.text = UserDefaultManager.getStringFromUserDefaults(key: UD_CartCount)
        }
        let urlString = BASE_URL
        let headers:NSDictionary = ["Content-type": "application/json"]
        let params: NSDictionary = ["theme_id":APP_THEME]
        self.Webservice_baseURL(url: urlString, params: params, header: headers)
    }
    
  @IBAction func btnTap_Search(_ sender: UIButton) {
    let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchVC") as! SearchVC
    self.navigationController?.pushViewController(vc, animated: true)
  }
    
    
}
// MARK: - button Action
extension HomeVC
{
    @IBAction func btnTap_ShowMore_Categories(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AllCategoriesVC") as! AllCategoriesVC
        vc.isnavigatehome = "1"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnTap_Menu(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
        vc.modalPresentationStyle = .fullScreen
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func btnTap_cart(_ sender: UIButton) {
//        let vc = self.storyboard?.instantiateViewController(identifier: "SearchVC") as! SearchVC
//        self.navigationController?.pushViewController(vc, animated: true)
        let vc = self.storyboard?.instantiateViewController(identifier: "CartVC") as! CartVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnTap_Loyaltiprogram(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoyalityprogramVC") as! LoyalityprogramVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
// MARK: - CollectionView numberOfItemsInSection
extension HomeVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.Collectionview_TrendingList {
            return self.Trending_Products_Array.count
        }
        if collectionView == self.Collectionview_FeaturedList {
            return self.Categories_Array.count + 1
        }
        if collectionView == self.Collectionview_FeaturedProductsList {
            return self.Featured_Products_Array.count
        }
        if collectionView == self.Collectionview_BestSellersList {
            return self.Bestseller_Products_Array.count
        }
        else {
            return 0
        }
    }
    
    // MARK: - CollectionView cellForItemAt
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.Collectionview_TrendingList {
            let cell = self.Collectionview_TrendingList.dequeueReusableCell(withReuseIdentifier: "featuredItemListCell", for: indexPath) as! featuredItemListCell
            let data = Trending_Products_Array[indexPath.item]
            cell.lbl_itemname.text = data["name"]!
            let ItemPrice = formatter.string(for: data["final_price"]!.toDouble)
            let ItemoriginalPrice = formatter.string(for: data["original_price"]!.toDouble)
            let attributeString: NSMutableAttributedString = NSMutableAttributedString(string: "\(ItemoriginalPrice!) \(UserDefaultManager.getStringFromUserDefaults(key: UD_currency_Name))")
            attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSRange(location: 0, length: attributeString.length))
            cell.lbl_itemdiscountPrice.attributedText = attributeString
            
            cell.lbl_itemPrice.text = ItemPrice
            cell.img_item.sd_setImage(with: URL(string: IMG_URL + data["cover_image_path"]!), placeholderImage: UIImage(named: ""))
            cornerRadius(viewName: cell.lbl_tag, radius: cell.lbl_tag.frame.height / 2)
            cell.lbl_tag.text = data["tag_api"]!.uppercased()
            if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == ""
            {
                cell.btn_favrites.isHidden = true
            }
            else
            {
                cell.btn_favrites.isHidden = false
            }
            if data["in_whishlist"]! == "false"
            {
                cell.btn_favrites.setImage(UIImage.init(named: "ic_hart"), for: .normal)
            }
            else{
                cell.btn_favrites.setImage(UIImage.init(named: "ic_hartfill"), for: .normal)
            }
            cell.lbl_currency.text = UserDefaultManager.getStringFromUserDefaults(key: UD_currency_Name)
            cell.btn_favrites.tag = indexPath.row
            cell.btn_favrites.addTarget(self, action: #selector(btnTap_Like_Trending), for: .touchUpInside)
            cell.btn_cart.tag = indexPath.row
            cell.btn_cart.addTarget(self, action: #selector(btnTap_Cart_Trending), for: .touchUpInside)
            return cell
        }
        else if collectionView == self.Collectionview_FeaturedList
        {
            let cell = self.Collectionview_FeaturedList.dequeueReusableCell(withReuseIdentifier: "featuredListCell", for: indexPath) as! featuredListCell
            if indexPath.row == 0
            {
                cell.lbl_title.text = "All Products"
            }
            else
            {
                let data = self.Categories_Array[indexPath.item - 1]
                cell.lbl_title.text = data["name"].stringValue
            }
            if indexPath.row == self.selectedindex
            {
                cell.cell_view.backgroundColor = UIColor.init(named: "App_bg_Color")
                cell.lbl_title.textColor = UIColor.white
                cornerRadius(viewName: cell.cell_view, radius: cell.cell_view.frame.height / 2)
            }
            else
            {
                cell.cell_view.backgroundColor = UIColor.init(named: "Gray_Color")
                cell.lbl_title.textColor = UIColor.white
                cornerRadius(viewName: cell.cell_view, radius: cell.cell_view.frame.height / 2)
            }
            
            return cell
        }
        else if collectionView == self.Collectionview_FeaturedProductsList
        {
            let cell = self.Collectionview_FeaturedProductsList.dequeueReusableCell(withReuseIdentifier: "featuredItemListCell", for: indexPath) as! featuredItemListCell
            let data = Featured_Products_Array[indexPath.item]
            cell.lbl_itemname.text = data["name"]!
            let ItemPrice = formatter.string(for: data["final_price"]!.toDouble)
            let ItemoriginalPrice = formatter.string(for: data["original_price"]!.toDouble)
            let attributeString: NSMutableAttributedString = NSMutableAttributedString(string: "\(ItemoriginalPrice!) \(UserDefaultManager.getStringFromUserDefaults(key: UD_currency_Name))")
            attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSRange(location: 0, length: attributeString.length))
            cell.lbl_itemdiscountPrice.attributedText = attributeString
            
            cell.lbl_itemPrice.text = ItemPrice
            cell.img_item.sd_setImage(with: URL(string: IMG_URL + data["cover_image_path"]!), placeholderImage: UIImage(named: ""))
            cornerRadius(viewName: cell.lbl_tag, radius: cell.lbl_tag.frame.height / 2)
            cell.lbl_tag.text = data["tag_api"]!.uppercased()
            if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == ""
            {
                cell.btn_favrites.isHidden = true
            }
            else
            {
                cell.btn_favrites.isHidden = false
            }
            if data["in_whishlist"]! == "false"
            {
                cell.btn_favrites.setImage(UIImage.init(named: "ic_hart"), for: .normal)
            }
            else{
                cell.btn_favrites.setImage(UIImage.init(named: "ic_hartfill"), for: .normal)
            }
            cell.lbl_currency.text = UserDefaultManager.getStringFromUserDefaults(key: UD_currency_Name)
            cell.btn_favrites.tag = indexPath.row
            cell.btn_favrites.addTarget(self, action: #selector(btnTap_Like), for: .touchUpInside)
            cell.btn_cart.tag = indexPath.row
            cell.btn_cart.addTarget(self, action: #selector(btnTap_Cart), for: .touchUpInside)
            return cell
        }
        else if collectionView == self.Collectionview_BestSellersList
        {
            let cell = self.Collectionview_BestSellersList.dequeueReusableCell(withReuseIdentifier: "featuredItemListCell", for: indexPath) as! featuredItemListCell
            let data = self.Bestseller_Products_Array[indexPath.item]
            cell.lbl_itemname.text = data["name"]!
            let ItemPrice = formatter.string(for: data["final_price"]!.toDouble)
            let ItemoriginalPrice = formatter.string(for: data["original_price"]!.toDouble)
            
            let attributeString: NSMutableAttributedString = NSMutableAttributedString(string: "\(ItemoriginalPrice!) \(UserDefaultManager.getStringFromUserDefaults(key: UD_currency_Name))")
            attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSRange(location: 0, length: attributeString.length))
            cell.lbl_itemdiscountPrice.attributedText = attributeString
            
            cell.lbl_itemPrice.text = ItemPrice
            cell.img_item.sd_setImage(with: URL(string: IMG_URL + data["cover_image_path"]!), placeholderImage: UIImage(named: ""))
            cornerRadius(viewName: cell.lbl_tag, radius: cell.lbl_tag.frame.height / 2)
            cell.lbl_tag.text = data["tag_api"]!.uppercased()
            if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == ""
            {
                cell.btn_favrites.isHidden = true
            }
            else{
                cell.btn_favrites.isHidden = false
            }
            if data["in_whishlist"]! == "false"
            {
                cell.btn_favrites.setImage(UIImage.init(named: "ic_hart"), for: .normal)
            }
            else{
                cell.btn_favrites.setImage(UIImage.init(named: "ic_hartfill"), for: .normal)
            }
            
            cell.lbl_currency.text = UserDefaultManager.getStringFromUserDefaults(key: UD_currency_Name)
            cell.btn_favrites.tag = indexPath.row
            cell.btn_favrites.addTarget(self, action: #selector(btnTap_Like_best), for: .touchUpInside)
            cell.btn_cart.tag = indexPath.row
            cell.btn_cart.addTarget(self, action: #selector(btnTap_Cart_best), for: .touchUpInside)
            return cell
        }
        else
        {
            return UICollectionViewCell()
        }
    }
    
    // MARK: - CollectionView layout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.Collectionview_TrendingList {
            return CGSize(width: (UIScreen.main.bounds.width - 54) / 2, height: ((UIScreen.main.bounds.width - 54) / 2) + 30)
        }
        else if collectionView == self.Collectionview_FeaturedList
        {
            return CGSize(width: (UIScreen.main.bounds.width - 54) / 2, height: 40)
        }
        else if collectionView == self.Collectionview_FeaturedProductsList
        {
            return CGSize(width: (UIScreen.main.bounds.width - 54) / 2, height: ((UIScreen.main.bounds.width - 54) / 2) + 30)
        }
        else if collectionView == self.Collectionview_BestSellersList {
            return CGSize(width: (UIScreen.main.bounds.width - 54) / 2, height: ((UIScreen.main.bounds.width - 54) / 2) + 30)
        }
        else {
            return CGSize.zero
        }
    }
    
    // MARK: - CollectionView didSelectItemAt
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.Collectionview_TrendingList  {
            let data = self.Trending_Products_Array[indexPath.item]
            let vc = self.storyboard?.instantiateViewController(identifier: "ItemDetailsVC") as! ItemDetailsVC
            vc.item_id = data["id"]!
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if collectionView == self.Collectionview_FeaturedList
        {
            
            if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == ""
            {
                if indexPath.item != 0
                {
                    self.pageIndex = 1
                    self.lastIndex = 0
                    let data = self.Categories_Array[indexPath.item - 1]
                    let urlString = API_URL + "categorys-product-guest?page=\(self.pageIndex)"
                    let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
                    self.SelectedCategoryid = data["maincategory_id"].stringValue
                    self.SelectedSubCategoryid = data["id"].stringValue
                    let params: NSDictionary = ["maincategory_id":self.SelectedCategoryid,"subcategory_id":self.SelectedSubCategoryid,"theme_id":APP_THEME]
                    self.Webservice_Categorysproduct(url: urlString, params: params, header: headers)
                }
                if indexPath.item == 0
                {
                    self.pageIndex = 1
                    self.lastIndex = 0
                    let urlString = API_URL + "categorys-product-guest?page=\(self.pageIndex)"
                    let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
                    self.SelectedCategoryid = ""
                    self.SelectedSubCategoryid = ""
                    let params: NSDictionary = ["maincategory_id":self.SelectedCategoryid,"subcategory_id":"","theme_id":APP_THEME]
                    self.Webservice_Categorysproduct(url: urlString, params: params, header: headers)
                }
            }
            else
            {
                if indexPath.item != 0
                {
                    self.pageIndex = 1
                    self.lastIndex = 0
                    let data = self.Categories_Array[indexPath.item - 1]
                    let urlString = API_URL + "categorys-product?page=\(self.pageIndex)"
                    let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
                    self.SelectedCategoryid = data["maincategory_id"].stringValue
                    self.SelectedSubCategoryid = data["id"].stringValue
                    let params: NSDictionary = ["maincategory_id":self.SelectedCategoryid,"subcategory_id":self.SelectedSubCategoryid,"theme_id":APP_THEME]
                    self.Webservice_Categorysproduct(url: urlString, params: params, header: headers)
                }
                if indexPath.item == 0
                {
                    self.pageIndex = 1
                    self.lastIndex = 0
                    let urlString = API_URL + "categorys-product?page=\(self.pageIndex)"
                    let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
                    self.SelectedCategoryid = ""
                    self.SelectedSubCategoryid = ""
                    let params: NSDictionary = ["maincategory_id":self.SelectedCategoryid,"subcategory_id":"","theme_id":APP_THEME]
                    self.Webservice_Categorysproduct(url: urlString, params: params, header: headers)
                }
            }
            
            self.selectedindex = indexPath.item
            self.Collectionview_FeaturedList.reloadData()
        }
        else if collectionView == self.Collectionview_FeaturedProductsList {
            let data = self.Featured_Products_Array[indexPath.item]
            let vc = self.storyboard?.instantiateViewController(identifier: "ItemDetailsVC") as! ItemDetailsVC
            vc.item_id = data["id"]!
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if collectionView == self.Collectionview_BestSellersList {
            let data = self.Bestseller_Products_Array[indexPath.item]
            let vc = self.storyboard?.instantiateViewController(identifier: "ItemDetailsVC") as! ItemDetailsVC
            vc.item_id = data["id"]!
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell,forItemAt indexPath: IndexPath)
    {
        if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == ""
        {
            if collectionView == self.Collectionview_FeaturedProductsList
            {
                if indexPath.item == self.Featured_Products_Array.count - 1 {
                    if self.pageIndex != self.lastIndex {
                        self.pageIndex = self.pageIndex + 1
                        if self.Featured_Products_Array.count != 0 {
                            let urlString = API_URL + "categorys-product-guest?page=\(self.pageIndex)"
                            let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
                            let params: NSDictionary = ["maincategory_id":self.SelectedCategoryid,"subcategory_id":self.SelectedSubCategoryid,"theme_id":APP_THEME]
                            self.Webservice_Categorysproduct(url: urlString, params: params, header: headers)
                        }
                    }
                }
                
            }
            else if collectionView == self.Collectionview_TrendingList
            {
                if indexPath.item == self.Trending_Products_Array.count - 1 {
                    if self.pageIndex_trending != self.lastIndex_trending {
                        self.pageIndex_trending = self.pageIndex_trending + 1
                        if self.Trending_Products_Array.count != 0 {
                            let urlString4 = API_URL + "tranding-category-product-guest?page=\(self.pageIndex_trending)"
                            let headers4:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
                            let params4: NSDictionary = ["theme_id":APP_THEME]
                            self.Webservice_Trendingprodcuts(url: urlString4, params: params4, header: headers4)
                        }
                    }
                }
                
            }
            
        }
        else{
            if collectionView == self.Collectionview_FeaturedProductsList
            {
                if indexPath.item == self.Featured_Products_Array.count - 1 {
                    if self.pageIndex != self.lastIndex {
                        self.pageIndex = self.pageIndex + 1
                        if self.Featured_Products_Array.count != 0 {
                            let urlString = API_URL + "categorys-product?page=\(self.pageIndex)"
                            let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
                            let params: NSDictionary = ["maincategory_id":self.SelectedCategoryid,"subcategory_id":self.SelectedSubCategoryid,"theme_id":APP_THEME]
                            self.Webservice_Categorysproduct(url: urlString, params: params, header: headers)
                        }
                    }
                }
            }
            else if collectionView == self.Collectionview_TrendingList
            {
                if indexPath.item == self.Trending_Products_Array.count - 1 {
                    if self.pageIndex_trending != self.lastIndex_trending {
                        self.pageIndex_trending = self.pageIndex_trending + 1
                        if self.Trending_Products_Array.count != 0 {
                            let urlString4 = API_URL + "tranding-category-product?page=\(self.pageIndex_trending)"
                            let headers4:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
                            let params4: NSDictionary = ["theme_id":APP_THEME]
                            self.Webservice_Trendingprodcuts(url: urlString4, params: params4, header: headers4)
                        }
                    }
                }
                
            }
        }
        
    }
    
    @objc func btnTap_Like(sender:UIButton) {
        if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == ""
        {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
            nav.navigationBar.isHidden = true
            keyWindow?.rootViewController = nav
        }
        else{
            let data = Featured_Products_Array[sender.tag]
            
            if data["in_whishlist"]! == "false"
            {
                let urlString = API_URL + "wishlist"
                let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
                let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),"product_id":data["id"]!,"wishlist_type":"add","theme_id":APP_THEME]
                self.Webservice_wishlist(url: urlString, params: params, header: headers, wishlisttype: "add", sender: sender.tag, isselect: "Featured")
            }
            else if data["in_whishlist"]! == "true"
            {
                let urlString = API_URL + "wishlist"
                let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
                let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),"product_id":data["id"]!,"wishlist_type":"remove","theme_id":APP_THEME]
                self.Webservice_wishlist(url: urlString, params: params, header: headers, wishlisttype: "remove", sender: sender.tag, isselect: "Featured")
            }
        }
        
    }
    @objc func btnTap_Cart(sender:UIButton) {
        if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == ""
        {
            let data = Featured_Products_Array[sender.tag]
            if UserDefaults.standard.value(forKey: UD_GuestObj) != nil
            {
                var Guest_Array = UserDefaultManager.getCustomObjFromUserDefaultsGuest(key: UD_GuestObj) as! [[String:String]]
                var iscart = false
                var cartindex = Int()
                for i in 0..<Guest_Array.count
                {
                    if Guest_Array[i]["product_id"]! == data["id"]! && Guest_Array[i]["variant_id"]! == data["default_variant_id"]!
                    {
                        iscart = true
                        cartindex = i
                    }
                    
                }
                if iscart == false
                {
                    let cartobj = ["product_id": data["id"]!,
                                   "image": data["cover_image_path"]!,
                                   "name": data["name"]!,
                                   "orignal_price": data["orignal_price"]!,
                                   "discount_price": data["discount_price"]!,
                                   "final_price": data["final_price"]!,
                                   "qty": "1",
                                   "variant_id": data["default_variant_id"]!,
                                   "variant_name": data["variant_name"]!]
                    Guest_Array.append(cartobj)
                    UserDefaultManager.setCustomObjToUserDefaultsGuest(CustomeObj: Guest_Array, key: UD_GuestObj)
                    UserDefaultManager.setStringToUserDefaults(value: "\(Guest_Array.count)", key: UD_CartCount)
                    
                    let alert = UIAlertController(title: nil, message: "\(data["name"]!) add successfully", preferredStyle: .alert)
                    let photoLibraryAction = UIAlertAction(title: "Continue shopping", style: .default) { (action) in
                        self.dismiss(animated: true)
                    }
                    
                    let cameraAction = UIAlertAction(title: "Proceed to check out", style: .default) { (action) in
                        let vc = self.storyboard?.instantiateViewController(identifier: "CartVC") as! CartVC
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    
                    // let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                    alert.addAction(photoLibraryAction)
                    alert.addAction(cameraAction)
                    // alert.addAction(cancelAction)
                    self.present(alert, animated: true, completion: nil)
                }
                else{
                    let alertVC = UIAlertController(title: Bundle.main.displayName!, message: ALREADYCART_CONFIRM_MESSAGE, preferredStyle: .alert)
                    let yesAction = UIAlertAction(title: "Yes", style: .default) { (action) in
                        
                        var data = Guest_Array[cartindex]
                        data["qty"] = "\(Int(data["qty"]!)! + 1)"
                        Guest_Array.remove(at: cartindex)
                        Guest_Array.insert(data, at: cartindex)
                        
                        UserDefaultManager.setCustomObjToUserDefaultsGuest(CustomeObj: Guest_Array, key: UD_GuestObj)
                        UserDefaultManager.setStringToUserDefaults(value: "\(Guest_Array.count)", key: UD_CartCount)
                        
                    }
                    let noAction = UIAlertAction(title: "No", style: .destructive)
                    alertVC.addAction(noAction)
                    alertVC.addAction(yesAction)
                    self.present(alertVC,animated: true,completion: nil)
                }
                
            }
            self.lbl_count.text = UserDefaultManager.getStringFromUserDefaults(key: UD_CartCount)
            
        }
        else{
            let data = self.Featured_Products_Array[sender.tag]
            self.product_id = data["id"]!
            self.Selected_Variant_id = data["default_variant_id"]!
            let urlString = API_URL + "addtocart"
            let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
            let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),"variant_id":data["default_variant_id"]!,"qty":"1","product_id":data["id"]!,"theme_id":APP_THEME]
            self.Webservice_Cart(url: urlString, params: params, header: headers)
        }
    }
    
    @objc func btnTap_Like_best(sender:UIButton) {
        if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == ""
        {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
            nav.navigationBar.isHidden = true
            keyWindow?.rootViewController = nav
        }
        else{
            let data = self.Bestseller_Products_Array[sender.tag]
            if data["in_whishlist"]! == "false"
            {
                let urlString = API_URL + "wishlist"
                let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
                let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),"product_id":data["id"]!,"wishlist_type":"add","theme_id":APP_THEME]
                self.Webservice_wishlist(url: urlString, params: params, header: headers, wishlisttype: "add", sender: sender.tag, isselect: "best")
            }
            else if data["in_whishlist"]! == "true"
            {
                let urlString = API_URL + "wishlist"
                let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
                let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),"product_id":data["id"]!,"wishlist_type":"remove","theme_id":APP_THEME]
                self.Webservice_wishlist(url: urlString, params: params, header: headers, wishlisttype: "remove", sender: sender.tag, isselect: "best")
            }
        }
    }
    
    @objc func btnTap_Cart_best(sender:UIButton) {
        if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == ""
        {
            let data = Bestseller_Products_Array[sender.tag]
            if UserDefaults.standard.value(forKey: UD_GuestObj) != nil
            {
                var Guest_Array = UserDefaultManager.getCustomObjFromUserDefaultsGuest(key: UD_GuestObj) as! [[String:String]]
                var iscart = false
                var cartindex = Int()
                for i in 0..<Guest_Array.count
                {
                    if Guest_Array[i]["product_id"]! == data["id"]! && Guest_Array[i]["variant_id"]! == data["default_variant_id"]!
                    {
                        iscart = true
                        cartindex = i
                    }
                    
                }
                if iscart == false
                {
                    let cartobj = ["product_id": data["id"]!,
                                   "image": data["cover_image_path"]!,
                                   "name": data["name"]!,
                                   "orignal_price": data["orignal_price"]!,
                                   "discount_price": data["discount_price"]!,
                                   "final_price": data["final_price"]!,
                                   "qty": "1",
                                   "variant_id": data["default_variant_id"]!,
                                   "variant_name": data["variant_name"]!]
                    Guest_Array.append(cartobj)
                    UserDefaultManager.setCustomObjToUserDefaultsGuest(CustomeObj: Guest_Array, key: UD_GuestObj)
                    UserDefaultManager.setStringToUserDefaults(value: "\(Guest_Array.count)", key: UD_CartCount)
                    
                    let alert = UIAlertController(title: nil, message: "\(data["name"]!) add successfully", preferredStyle: .alert)
                    let photoLibraryAction = UIAlertAction(title: "Continue shopping", style: .default) { (action) in
                        self.dismiss(animated: true)
                    }
                    
                    let cameraAction = UIAlertAction(title: "Proceed to check out", style: .default) { (action) in
                        let vc = self.storyboard?.instantiateViewController(identifier: "CartVC") as! CartVC
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    
                    //let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                    alert.addAction(photoLibraryAction)
                    alert.addAction(cameraAction)
                    //alert.addAction(cancelAction)
                    self.present(alert, animated: true, completion: nil)
                }
                else{
                    let alertVC = UIAlertController(title: Bundle.main.displayName!, message: ALREADYCART_CONFIRM_MESSAGE, preferredStyle: .alert)
                    let yesAction = UIAlertAction(title: "Yes", style: .default) { (action) in
                        
                        var data = Guest_Array[cartindex]
                        data["qty"] = "\(Int(data["qty"]!)! + 1)"
                        Guest_Array.remove(at: cartindex)
                        Guest_Array.insert(data, at: cartindex)
                        
                        UserDefaultManager.setCustomObjToUserDefaultsGuest(CustomeObj: Guest_Array, key: UD_GuestObj)
                        UserDefaultManager.setStringToUserDefaults(value: "\(Guest_Array.count)", key: UD_CartCount)
                        
                    }
                    let noAction = UIAlertAction(title: "No", style: .destructive)
                    alertVC.addAction(noAction)
                    alertVC.addAction(yesAction)
                    self.present(alertVC,animated: true,completion: nil)
                }
            }
            self.lbl_count.text = UserDefaultManager.getStringFromUserDefaults(key: UD_CartCount)
            
        }
        else{
            let data = self.Bestseller_Products_Array[sender.tag]
            self.product_id = data["id"]!
            self.Selected_Variant_id = data["default_variant_id"]!
            let urlString = API_URL + "addtocart"
            let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
            let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),"variant_id":data["default_variant_id"]!,"qty":"1","product_id":data["id"]!,"theme_id":APP_THEME]
            self.Webservice_Cart(url: urlString, params: params, header: headers)
        }
    }
    @objc func btnTap_Like_Trending(sender:UIButton) {
        if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == ""
        {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
            nav.navigationBar.isHidden = true
            keyWindow?.rootViewController = nav
        }
        else{
            let data = self.Trending_Products_Array[sender.tag]
            if data["in_whishlist"]! == "false"
            {
                let urlString = API_URL + "wishlist"
                let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
                let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),"product_id":data["id"]!,"wishlist_type":"add","theme_id":APP_THEME]
                self.Webservice_wishlist(url: urlString, params: params, header: headers, wishlisttype: "add", sender: sender.tag, isselect: "trending")
            }
            else if data["in_whishlist"]! == "true"
            {
                let urlString = API_URL + "wishlist"
                let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
                let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),"product_id":data["id"]!,"wishlist_type":"remove","theme_id":APP_THEME]
                self.Webservice_wishlist(url: urlString, params: params, header: headers, wishlisttype: "remove", sender: sender.tag, isselect: "trending")
            }
        }
    }
    
    @objc func btnTap_Cart_Trending(sender:UIButton) {
        if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == ""
        {
            let data = Trending_Products_Array[sender.tag]
            if UserDefaults.standard.value(forKey: UD_GuestObj) != nil
            {
                var Guest_Array = UserDefaultManager.getCustomObjFromUserDefaultsGuest(key: UD_GuestObj) as! [[String:String]]
                var iscart = false
                var cartindex = Int()
                for i in 0..<Guest_Array.count
                {
                    if Guest_Array[i]["product_id"]! == data["id"]! && Guest_Array[i]["variant_id"]! == data["default_variant_id"]!
                    {
                        iscart = true
                        cartindex = i
                    }
                    
                }
                if iscart == false
                {
                    let cartobj = ["product_id": data["id"]!,
                                   "image": data["cover_image_path"]!,
                                   "name": data["name"]!,
                                   "orignal_price": data["orignal_price"]!,
                                   "discount_price": data["discount_price"]!,
                                   "final_price": data["final_price"]!,
                                   "qty": "1",
                                   "variant_id": data["default_variant_id"]!,
                                   "variant_name": data["variant_name"]!]
                    Guest_Array.append(cartobj)
                    UserDefaultManager.setCustomObjToUserDefaultsGuest(CustomeObj: Guest_Array, key: UD_GuestObj)
                    UserDefaultManager.setStringToUserDefaults(value: "\(Guest_Array.count)", key: UD_CartCount)
                    
                    let alert = UIAlertController(title: nil, message: "\(data["name"]!) add successfully", preferredStyle: .alert)
                    let photoLibraryAction = UIAlertAction(title: "Continue shopping", style: .default) { (action) in
                        self.dismiss(animated: true)
                    }
                    
                    let cameraAction = UIAlertAction(title: "Proceed to check out", style: .default) { (action) in
                        let vc = self.storyboard?.instantiateViewController(identifier: "CartVC") as! CartVC
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    
                    //let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                    alert.addAction(photoLibraryAction)
                    alert.addAction(cameraAction)
                    // alert.addAction(cancelAction)
                    self.present(alert, animated: true, completion: nil)
                }
                else{
                    let alertVC = UIAlertController(title: Bundle.main.displayName!, message: ALREADYCART_CONFIRM_MESSAGE, preferredStyle: .alert)
                    let yesAction = UIAlertAction(title: "Yes", style: .default) { (action) in
                        
                        var data = Guest_Array[cartindex]
                        data["qty"] = "\(Int(data["qty"]!)! + 1)"
                        Guest_Array.remove(at: cartindex)
                        Guest_Array.insert(data, at: cartindex)
                        
                        UserDefaultManager.setCustomObjToUserDefaultsGuest(CustomeObj: Guest_Array, key: UD_GuestObj)
                        UserDefaultManager.setStringToUserDefaults(value: "\(Guest_Array.count)", key: UD_CartCount)
                        
                    }
                    let noAction = UIAlertAction(title: "No", style: .destructive)
                    alertVC.addAction(noAction)
                    alertVC.addAction(yesAction)
                    self.present(alertVC,animated: true,completion: nil)
                }
            }
            self.lbl_count.text = UserDefaultManager.getStringFromUserDefaults(key: UD_CartCount)
            
        }
        else{
            let data = self.Trending_Products_Array[sender.tag]
            self.product_id = data["id"]!
            self.Selected_Variant_id = data["default_variant_id"]!
            let urlString = API_URL + "addtocart"
            let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
            let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),"variant_id":data["default_variant_id"]!,"qty":"1","product_id":data["id"]!,"theme_id":APP_THEME]
            self.Webservice_Cart(url: urlString, params: params, header: headers)
        }
    }
}

//MARK: Tableview Methods
extension HomeVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.Home_Categories_Array.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.Tableview_CategoriesList.dequeueReusableCell(withIdentifier: "AllCategoriesCell") as! AllCategoriesCell
        let data = self.Home_Categories_Array[indexPath.row]
        cell.lbl_categories.text = data["name"]!
        cell.img_catimage.sd_setImage(with: URL(string: IMG_URL + data["image_path"]!), placeholderImage: UIImage(named: "ic_placeholder"))
        cell.btn_Find.tag = indexPath.row
        cell.btn_Find.addTarget(self, action: #selector(btnTap_Find), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let data = self.Home_Categories_Array[indexPath.item]
        let vc = self.storyboard?.instantiateViewController(identifier: "AllProductsVC") as! AllProductsVC
        vc.maincategory_id = data["id"]!
        vc.ishome = "yes"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func btnTap_Find(sender:UIButton) {
        let data = self.Home_Categories_Array[sender.tag]
        let vc = self.storyboard?.instantiateViewController(identifier: "AllProductsVC") as! AllProductsVC
        vc.maincategory_id = data["id"]!
        vc.ishome = "yes"
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}
extension HomeVC : ProductListDelegate
{
    func getdata(subcategory_id: String, maincategory_id: String,categories_name: String) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BestsellersVC") as! BestsellersVC
        vc.isNavigetMenu = "1"
        vc.subcategory_id = subcategory_id
        vc.maincategory_id = maincategory_id
        vc.MainCategorie = categories_name
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
extension HomeVC {
    
    func Webservice_baseURL(url:String, params:NSDictionary,header:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: header, parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ statusCode:String) in
            let status = jsonResponse!["status"].stringValue
            if status == "1"
            {
                let jsondata = jsonResponse!["data"].dictionaryValue
                IMG_URL = jsondata["image_url"]!.stringValue
                API_URL = "\(jsondata["base_url"]!.stringValue)/"
                PAYMENT_URL = "\(jsondata["payment_url"]!.stringValue)/"
                
                let urlString = API_URL + "landingpage"
                let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
                let params: NSDictionary = ["theme_id":APP_THEME]
                self.Webservice_landingpage(url: urlString, params: params, header: headers)
                
            }
            else if status == "9"
            {
                UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userId)
                UserDefaultManager.setStringToUserDefaults(value: "", key: UD_BearerToken)
                UserDefaultManager.setStringToUserDefaults(value: "", key: UD_TokenType)
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                nav.navigationBar.isHidden = true
                keyWindow?.rootViewController = nav
            }
            else
            {
                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: jsonResponse!["data"]["message"].stringValue.replacingOccurrences(of: "\\n", with: "\n"))
            }
        }
    }
    
    // MARK: - landing page api calling
    func Webservice_landingpage(url:String, params:NSDictionary, header:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: header, parameters: params, httpMethod: "POST", progressView: true, uiView: self.view, networkAlert: true)
        {(_ jsonResponse:JSON? , _ statusCode:String) in
            let status = jsonResponse!["status"].stringValue
            if status == "1" {
                let jsondata = jsonResponse!["data"]["them_json"].dictionaryValue
                
                // home-header
                let home_header = jsondata["home-header"]!.dictionaryValue
                self.home_header_bg_image.sd_setImage(with: URL(string: IMG_URL + home_header["home-header-bg-image"]!.stringValue), placeholderImage: UIImage(named: ""))
                self.home_header_title_text.text = home_header["home-header-title-text"]!.stringValue
                self.home_header_sub_text.text = home_header["home-header-sub-text"]!.stringValue
                
                // home-featured-product
                let home_featured_product = jsondata["home-featured-product"]!.dictionaryValue
                self.home_featured_product_title.text = home_featured_product["home-featured-product-title"]!.stringValue
                self.home_featured_product_text.text = home_featured_product["home-featured-product-text"]!.stringValue
                
                // home-categories
                let home_categories_product = jsondata["home-categories"]!.dictionaryValue
                self.home_categories_title.text = home_categories_product["home-categories-title"]!.stringValue
                self.home_categories_text.text = home_categories_product["home-categories-text"]!.stringValue
                self.home_categories_button.setTitle(home_categories_product["home-categories-button"]!.stringValue, for: .normal)
                
                // home-bestsellers
                let home_bestseller = jsondata["home-bestsellers"]!.dictionaryValue
                self.home_bestsellers_title.text = home_bestseller["home-bestsellers-title"]!.stringValue
                self.home_bestsellers_text.text = home_bestseller["home-bestsellers-text"]!.stringValue
                self.home_bestsellers_button.setTitle(home_bestseller["home-bestsellers-button"]!.stringValue, for: .normal)
                
                // home-loyalty-program
                let home_loyalty_program = jsondata["home-loyalty-program"]!.dictionaryValue
                self.home_loyalty_program_title.text = home_loyalty_program["home-loyalty-program-title"]!.stringValue
                self.home_loyalty_program_text.text = home_loyalty_program["home-loyalty-program-text"]!.stringValue
                self.home_loyalty_program_button.setTitle(home_loyalty_program["home-loyalty-program-button"]!.stringValue, for: .normal)
                self.home_loyalty_program_sub_text.text = home_loyalty_program["home-loyalty-program-sub-text"]!.stringValue
                self.home_loyalty_program_bg_image.sd_setImage(with: URL(string: IMG_URL + home_loyalty_program["home-loyalty-program-bg-image"]!.stringValue), placeholderImage: UIImage(named: ""))
                
                
                let LoyaltyProgram = jsonResponse!["data"]["loyality_section"].stringValue
                if LoyaltyProgram == "on"
                {
                    self.Height_ViewLoyalti.constant = 250.0
                }
                else{
                    self.Height_ViewLoyalti.constant = 0.0
                }
                
                //------------------------------API CALL ----------------------------------------//
                
                let urlString1 = API_URL + "currency"
                let headers1:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
                let params1: NSDictionary = ["theme_id":APP_THEME]
                self.Webservice_currency(url: urlString1, params: params1, header: headers1)
                
                if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == ""
                {
                    self.pageIndex = 1
                    self.lastIndex = 0
                    let urlString = API_URL + "categorys-product-guest?page=\(self.pageIndex)"
                    let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
                    let params: NSDictionary = ["maincategory_id":self.SelectedCategoryid,"subcategory_id":self.SelectedSubCategoryid,"theme_id":APP_THEME]
                    self.Webservice_Categorysproduct(url: urlString, params: params, header: headers)
                    
                    
                    self.pageIndex_best = 1
                    self.lastIndex_best = 0
                    let urlString4 = API_URL + "bestseller-guest?page=\(self.pageIndex_best)"
                    let headers4:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
                    let params4: NSDictionary = ["theme_id":APP_THEME]
                    self.Webservice_Bestsellerprodcuts(url: urlString4, params: params4, header: headers4)
                    
                    self.pageIndex_trending = 1
                    self.lastIndex_trending = 0
                    let urlString5 = API_URL + "tranding-category-product-guest?page=\(self.pageIndex_trending)"
                    let headers5:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
                    let params5: NSDictionary = ["theme_id":APP_THEME]
                    self.Webservice_Trendingprodcuts(url: urlString5, params: params5, header: headers5)
                    
                    
                    let urlString2 = API_URL + "featured-products-guest"
                    let headers2:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
                    let params2: NSDictionary = ["theme_id":APP_THEME]
                    self.Webservice_category(url: urlString2, params: params2, header: headers2)
                }
                else{
                    self.pageIndex = 1
                    self.lastIndex = 0
                    let urlString = API_URL + "categorys-product?page=\(self.pageIndex)"
                    let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
                    let params: NSDictionary = ["maincategory_id":self.SelectedCategoryid,"subcategory_id":self.SelectedSubCategoryid,"theme_id":APP_THEME]
                    self.Webservice_Categorysproduct(url: urlString, params: params, header: headers)
                    
                    self.pageIndex_best = 1
                    self.lastIndex_best = 0
                    let urlString4 = API_URL + "bestseller?page=\(self.pageIndex_best)"
                    let headers4:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
                    let params4: NSDictionary = ["theme_id":APP_THEME]
                    self.Webservice_Bestsellerprodcuts(url: urlString4, params: params4, header: headers4)
                    
                    self.pageIndex_trending = 1
                    self.lastIndex_trending = 0
                    let urlString5 = API_URL + "tranding-category-product?page=\(self.pageIndex_trending)"
                    let headers5:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
                    let params5: NSDictionary = ["theme_id":APP_THEME]
                    self.Webservice_Trendingprodcuts(url: urlString5, params: params5, header: headers5)
                    
                    let urlString2 = API_URL + "featured-products"
                    let headers2:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
                    let params2: NSDictionary = ["theme_id":APP_THEME]
                    self.Webservice_category(url: urlString2, params: params2, header: headers2)
                }
                
                self.pageIndex_maincategory = 1
                self.lastIndex_maincategory = 0
                let urlString3 = API_URL + "home-categoty?page=\(self.pageIndex_maincategory)"
                let headers3:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
                let params3: NSDictionary = ["theme_id":APP_THEME]
                self.Webservice_Homecategory(url: urlString3, params: params3, header: headers3)
            }
        }
    }
    func Webservice_currency(url:String, params:NSDictionary,header:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: header, parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ statusCode:String) in
            let status = jsonResponse!["status"].stringValue
            if status == "1"
            {
                let jsondata = jsonResponse!["data"].dictionaryValue
                UserDefaultManager.setStringToUserDefaults(value: jsondata["currency"]!.stringValue, key: UD_currency)
                UserDefaultManager.setStringToUserDefaults(value: jsondata["currency_name"]!.stringValue, key: UD_currency_Name)
            }
            else if status == "9"
            {
                UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userId)
                UserDefaultManager.setStringToUserDefaults(value: "", key: UD_BearerToken)
                UserDefaultManager.setStringToUserDefaults(value: "", key: UD_TokenType)
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                nav.navigationBar.isHidden = true
                keyWindow?.rootViewController = nav
            }
            else
            {
                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: jsonResponse!["data"]["message"].stringValue.replacingOccurrences(of: "\\n", with: "\n"))
            }
        }
    }
    // MARK: - categories api calling
    func Webservice_category(url:String, params:NSDictionary, header:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: header, parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ statusCode:String) in
            let status = jsonResponse!["status"].stringValue
            if status == "1" {
                let jsondata = jsonResponse!["data"].arrayValue
                self.Categories_Array = jsondata
                self.Collectionview_FeaturedList.delegate = self
                self.Collectionview_FeaturedList.dataSource = self
                self.Collectionview_FeaturedList.reloadData()
            }
            else if status == "9" {
                UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userId)
                UserDefaultManager.setStringToUserDefaults(value: "", key: UD_BearerToken)
                UserDefaultManager.setStringToUserDefaults(value: "", key: UD_TokenType)
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                nav.navigationBar.isHidden = true
                keyWindow?.rootViewController = nav
            }
            else {
                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: jsonResponse!["data"]["message"].stringValue.replacingOccurrences(of: "\\n", with: "\n"))
            }
        }
    }
    func Webservice_Categorysproduct(url:String, params:NSDictionary,header:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: header, parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ statusCode:String) in
            let status = jsonResponse!["status"].stringValue
            if status == "1"
            {
                let jsondata = jsonResponse!["data"].dictionaryValue
                if self.pageIndex == 1 {
                    self.lastIndex = Int(jsondata["last_page"]!.stringValue)!
                    self.Featured_Products_Array.removeAll()
                }
                let Featuredprodcutdata = jsondata["data"]!.arrayValue
                for data in Featuredprodcutdata
                {
                    
                    let productObj = ["id":data["id"].stringValue,"name":data["name"].stringValue,"tag_api":data["tag_api"].stringValue,"cover_image_path":data["cover_image_path"].stringValue,"final_price":data["final_price"].stringValue,"in_whishlist":data["in_whishlist"].stringValue,"default_variant_id":data["default_variant_id"].stringValue,"orignal_price":data["original_price"].stringValue,"discount_price":data["discount_price"].stringValue,"variant_name":data["default_variant_name"].stringValue,"original_price":data["original_price"].stringValue]
                    
                    self.Featured_Products_Array.append(productObj)
                }
                
                self.Collectionview_FeaturedProductsList.delegate = self
                self.Collectionview_FeaturedProductsList.dataSource = self
                self.Collectionview_FeaturedProductsList.reloadData()
                
            }
            else if status == "9"
            {
                UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userId)
                UserDefaultManager.setStringToUserDefaults(value: "", key: UD_BearerToken)
                UserDefaultManager.setStringToUserDefaults(value: "", key: UD_TokenType)
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                nav.navigationBar.isHidden = true
                keyWindow?.rootViewController = nav
            }
            else
            {
                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: jsonResponse!["data"]["message"].stringValue.replacingOccurrences(of: "\\n", with: "\n"))
            }
        }
    }
    func Webservice_Homecategory(url:String, params:NSDictionary, header:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: header, parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ statusCode:String) in
            let status = jsonResponse!["status"].stringValue
            if status == "1" {
                
                let jsondata = jsonResponse!["data"].dictionaryValue
                if self.pageIndex_maincategory == 1 {
                    self.lastIndex_maincategory = Int(jsondata["last_page"]!.stringValue)!
                    self.Home_Categories_Array.removeAll()
                }
                let Featuredprodcutdata = jsondata["data"]!.arrayValue
                for data in Featuredprodcutdata {
                    let productObj = ["id":data["id"].stringValue,"name":data["name"].stringValue,"image_path":data["image_path"].stringValue,"status":data["status"].stringValue,"category_id":data["category_id"].stringValue,"category_item":data["category_item"].stringValue]
                    self.Home_Categories_Array.append(productObj)
                }
                self.Tableview_CategoriesList.reloadData()
                self.Tableview_CategoriesList.delegate = self
                self.Tableview_CategoriesList.dataSource = self
                self.Height_Tableview.constant = CGFloat(self.Home_Categories_Array.count * 200)
                if self.Home_Categories_Array.count % 2 == 0 {
                    //self.Height_CategoriesCollectionview.constant = CGFloat((self.Home_Categories_Array.count / 2)) * 228
                }
                else{
                    //self.Height_CategoriesCollectionview.constant = CGFloat((((self.Home_Categories_Array.count - 1) / 2) + 1)) * 228
                }
                if self.pageIndex_maincategory == self.lastIndex_maincategory  {
                    //self.btn_bestShowMore_Categories.isHidden = true
                    //self.Height_CategoriesShowMore.constant = 0.0
                    //self.Height_ShowMoretopCategories.constant = 0.0
                }
                else{
                    //self.btn_bestShowMore_Categories.isHidden = false
                    //self.Height_CategoriesShowMore.constant = 30.0
                    //self.Height_ShowMoretopCategories.constant = 20.0
                }
                
            }
            else if status == "9"
            {
                UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userId)
                UserDefaultManager.setStringToUserDefaults(value: "", key: UD_BearerToken)
                UserDefaultManager.setStringToUserDefaults(value: "", key: UD_TokenType)
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                nav.navigationBar.isHidden = true
                keyWindow?.rootViewController = nav
            }
            else
            {
                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: jsonResponse!["data"]["message"].stringValue.replacingOccurrences(of: "\\n", with: "\n"))
            }
        }
    }
    func Webservice_Bestsellerprodcuts(url:String, params:NSDictionary,header:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: header, parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ statusCode:String) in
            let status = jsonResponse!["status"].stringValue
            if status == "1" {
                let jsondata = jsonResponse!["data"].dictionaryValue
                if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == ""  {
                    self.lbl_count.text = UserDefaultManager.getStringFromUserDefaults(key: UD_CartCount)
                }
                else{
                    
                    UserDefaultManager.setStringToUserDefaults(value: jsonResponse!["count"].stringValue, key: UD_CartCount)
                    self.lbl_count.text = UserDefaultManager.getStringFromUserDefaults(key: UD_CartCount)
                }
                if self.pageIndex_best == 1 {
                    
                    self.lastIndex_best = Int(jsondata["last_page"]!.stringValue)!
                     self.Bestseller_Products_Array.removeAll()
                }
                if self.pageIndex_best == self.lastIndex_best {
                    //self.btn_bestShowMore.isHidden = true
                    //self.Height_ShowMoretopBest.constant = 0.0
                    //self.Height_BestShowMore.constant = 0.0
                }
                else{
                    
                    //self.btn_bestShowMore.isHidden = false
                    //self.Height_ShowMoretopBest.constant = 20.0
                    //self.Height_BestShowMore.constant = 30.0
                }
                let Featuredprodcutdata = jsondata["data"]!.arrayValue
                for data in Featuredprodcutdata  {
                    let productObj = ["id":data["id"].stringValue,"name":data["name"].stringValue,"tag_api":data["tag_api"].stringValue,"cover_image_path":data["cover_image_path"].stringValue,"final_price":data["final_price"].stringValue,"in_whishlist":data["in_whishlist"].stringValue,"default_variant_id":data["default_variant_id"].stringValue,"orignal_price":data["original_price"].stringValue,"discount_price":data["discount_price"].stringValue,"variant_name":data["default_variant_name"].stringValue,"original_price":data["original_price"].stringValue]
                    self.Bestseller_Products_Array.append(productObj)
                }
                
                self.Collectionview_BestSellersList.delegate = self
                self.Collectionview_BestSellersList.dataSource = self
                self.Collectionview_BestSellersList.reloadData()
                self.Collectionview_FeaturedList.delegate = self
                self.Collectionview_FeaturedList.dataSource = self
                self.Collectionview_FeaturedList.reloadData()
                
                if self.Bestseller_Products_Array.count % 2 == 0  {
                    //self.Height_BestsellersCollectionview.constant = CGFloat((self.Bestseller_Products_Array.count / 2)) * (((UIScreen.main.bounds.width - 54) / 2) + 78)
                }
                else{
                    //self.Height_BestsellersCollectionview.constant = CGFloat((((self.Bestseller_Products_Array.count - 1) / 2) + 1)) * (((UIScreen.main.bounds.width - 54) / 2) + 78)
                }
                //print("-------------------\(self.Bestseller_Products_Array.count)-------------------")
                let urlString = API_URL + "extra-url"
                let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
                let params: NSDictionary = ["theme_id":APP_THEME]
                print(params)
                print(headers)
                self.Webservice_Extraurl(url: urlString, params: params, header: headers)
                self.view_Empty.isHidden = true
            }
            else if status == "9" {
                UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userId)
                UserDefaultManager.setStringToUserDefaults(value: "", key: UD_BearerToken)
                UserDefaultManager.setStringToUserDefaults(value: "", key: UD_TokenType)
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                nav.navigationBar.isHidden = true
                keyWindow?.rootViewController = nav
            }
            else {
                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: jsonResponse!["data"]["message"].stringValue.replacingOccurrences(of: "\\n", with: "\n"))
            }
        }
    }
    func Webservice_Extraurl(url:String, params:NSDictionary,header:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: header, parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ statusCode:String) in
            let status = jsonResponse!["status"].stringValue
            if status == "1"
            {
                let jsondata = jsonResponse!["data"].dictionaryValue
                print(jsondata)
                UserDefaultManager.setStringToUserDefaults(value: jsondata["contact_us"]!.stringValue, key: UD_ContactusURL)
                UserDefaultManager.setStringToUserDefaults(value: jsondata["terms"]!.stringValue, key: UD_TermsURL)
                
                UserDefaultManager.setStringToUserDefaults(value: jsondata["youtube"]!.stringValue, key: UD_YoutubeURL)
                UserDefaultManager.setStringToUserDefaults(value: jsondata["messanger"]!.stringValue, key: UD_MessageURL)
                UserDefaultManager.setStringToUserDefaults(value: jsondata["insta"]!.stringValue, key: UD_InstaURL)
                UserDefaultManager.setStringToUserDefaults(value: jsondata["twitter"]!.stringValue, key: UD_TwitterURL)
                UserDefaultManager.setStringToUserDefaults(value: jsondata["return_policy"]!.stringValue, key: UD_ReturnPolicyURL)
                self.view_Empty.isHidden = true
            }
            else if status == "9"
            {
                UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userId)
                UserDefaultManager.setStringToUserDefaults(value: "", key: UD_BearerToken)
                UserDefaultManager.setStringToUserDefaults(value: "", key: UD_TokenType)
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                nav.navigationBar.isHidden = true
                keyWindow?.rootViewController = nav
            }
            else
            {
                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: jsonResponse!["data"]["message"].stringValue.replacingOccurrences(of: "\\n", with: "\n"))
            }
        }
    }
    func Webservice_wishlist(url:String, params:NSDictionary,header:NSDictionary,wishlisttype:String,sender:Int,isselect:String) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: header, parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ statusCode:String) in
            let status = jsonResponse!["status"].stringValue
            if status == "1"
            {
                if isselect == "best"
                {
                    if wishlisttype == "add"
                    {
                        var data = self.Bestseller_Products_Array[sender]
                        data["in_whishlist"]! = "true"
                        self.Bestseller_Products_Array.remove(at: sender)
                        self.Bestseller_Products_Array.insert(data, at: sender)
                        self.Collectionview_BestSellersList.reloadData()
                    }
                    else
                    {
                        var data = self.Bestseller_Products_Array[sender]
                        data["in_whishlist"]! = "false"
                        self.Bestseller_Products_Array.remove(at: sender)
                        self.Bestseller_Products_Array.insert(data, at: sender)
                        self.Collectionview_BestSellersList.reloadData()
                    }
                }
                if isselect == "trending"
                {
                    if wishlisttype == "add"
                    {
                        var data = self.Trending_Products_Array[sender]
                        data["in_whishlist"]! = "true"
                        self.Trending_Products_Array.remove(at: sender)
                        self.Trending_Products_Array.insert(data, at: sender)
                        self.Collectionview_TrendingList.reloadData()
                    }
                    else
                    {
                        var data = self.Trending_Products_Array[sender]
                        data["in_whishlist"]! = "false"
                        self.Trending_Products_Array.remove(at: sender)
                        self.Trending_Products_Array.insert(data, at: sender)
                        self.Collectionview_TrendingList.reloadData()
                    }
                }
                else if isselect == "Featured"{
                    if wishlisttype == "add"
                    {
                        var data = self.Featured_Products_Array[sender]
                        data["in_whishlist"]! = "true"
                        self.Featured_Products_Array.remove(at: sender)
                        self.Featured_Products_Array.insert(data, at: sender)
                        self.Collectionview_FeaturedProductsList.reloadData()
                    }
                    else
                    {
                        var data = self.Featured_Products_Array[sender]
                        data["in_whishlist"]! = "false"
                        self.Featured_Products_Array.remove(at: sender)
                        self.Featured_Products_Array.insert(data, at: sender)
                        self.Collectionview_FeaturedProductsList.reloadData()
                    }
                }
            }
            else if status == "9"
            {
                UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userId)
                UserDefaultManager.setStringToUserDefaults(value: "", key: UD_BearerToken)
                UserDefaultManager.setStringToUserDefaults(value: "", key: UD_TokenType)
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                nav.navigationBar.isHidden = true
                keyWindow?.rootViewController = nav
            }
            else
            {
                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: jsonResponse!["data"]["message"].stringValue.replacingOccurrences(of: "\\n", with: "\n"))
            }
        }
        
    }
    func Webservice_Cart(url:String, params:NSDictionary,header:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: header, parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ statusCode:String) in
            let status = jsonResponse!["status"].stringValue
            if status == "1"
            {
                //showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: jsonResponse!["data"]["message"].stringValue.replacingOccurrences(of: "\\n", with: "\n"))
                UserDefaultManager.setStringToUserDefaults(value: jsonResponse!["data"]["count"].stringValue, key: UD_CartCount)
                self.lbl_count.text = UserDefaultManager.getStringFromUserDefaults(key: UD_CartCount)
                let alert = UIAlertController(title: nil, message: jsonResponse!["data"]["message"].stringValue.replacingOccurrences(of: "\\n", with: "\n"), preferredStyle: .alert)
                let photoLibraryAction = UIAlertAction(title: "Continue shopping", style: .default) { (action) in
                    self.dismiss(animated: true)
                }
                let cameraAction = UIAlertAction(title: "Proceed to check out", style: .default) { (action) in
                    let vc = self.storyboard?.instantiateViewController(identifier: "CartVC") as! CartVC
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                //let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                alert.addAction(photoLibraryAction)
                alert.addAction(cameraAction)
                //alert.addAction(cancelAction)
                self.present(alert, animated: true, completion: nil)
            }
            else if status == "9"
            {
                UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userId)
                UserDefaultManager.setStringToUserDefaults(value: "", key: UD_BearerToken)
                UserDefaultManager.setStringToUserDefaults(value: "", key: UD_TokenType)
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                nav.navigationBar.isHidden = true
                keyWindow?.rootViewController = nav
            }
            else if status == "0"
            {
                let alertVC = UIAlertController(title: Bundle.main.displayName!, message: ALREADYCART_CONFIRM_MESSAGE, preferredStyle: .alert)
                let yesAction = UIAlertAction(title: "Yes", style: .default) { (action) in
                    
                    let urlString = API_URL + "cart-qty"
                    let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
                    // quantity_type :- increase | decrease | remove (remove from cart)
                    let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),"product_id":self.product_id,"variant_id":self.Selected_Variant_id,"quantity_type":"increase","theme_id":APP_THEME]
                    self.Webservice_CartQty(url: urlString, params: params, header: headers)
                    
                }
                let noAction = UIAlertAction(title: "No", style: .destructive)
                alertVC.addAction(noAction)
                alertVC.addAction(yesAction)
                self.present(alertVC,animated: true,completion: nil)
            }
            else
            {
                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: jsonResponse!["data"]["message"].stringValue.replacingOccurrences(of: "\\n", with: "\n"))
            }
        }
    }
    func Webservice_CartQty(url:String, params:NSDictionary,header:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: header, parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ statusCode:String) in
            let status = jsonResponse!["status"].stringValue
            if status == "1"
            {
                UserDefaultManager.setStringToUserDefaults(value: jsonResponse!["count"].stringValue, key: UD_CartCount)
                
            }
            else if status == "9"
            {
                UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userId)
                UserDefaultManager.setStringToUserDefaults(value: "", key: UD_BearerToken)
                UserDefaultManager.setStringToUserDefaults(value: "", key: UD_TokenType)
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                nav.navigationBar.isHidden = true
                keyWindow?.rootViewController = nav
            }
            else
            {
                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: jsonResponse!["data"]["message"].stringValue.replacingOccurrences(of: "\\n", with: "\n"))
            }
        }
    }
    func Webservice_Trendingprodcuts(url:String, params:NSDictionary,header:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: header, parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ statusCode:String) in
            let status = jsonResponse!["status"].stringValue
            if status == "1"
            {
                let jsondata = jsonResponse!["data"].dictionaryValue
                if self.pageIndex_trending == 1 {
                    
                    self.lastIndex_trending = Int(jsondata["last_page"]!.stringValue)!
                    self.Trending_Products_Array.removeAll()
                }
                
                let Featuredprodcutdata = jsondata["data"]!.arrayValue
                for data in Featuredprodcutdata
                {
                    let productObj = ["id":data["id"].stringValue,"name":data["name"].stringValue,"tag_api":data["tag_api"].stringValue,"cover_image_path":data["cover_image_path"].stringValue,"final_price":data["final_price"].stringValue,"in_whishlist":data["in_whishlist"].stringValue,"default_variant_id":data["default_variant_id"].stringValue,"orignal_price":data["original_price"].stringValue,"discount_price":data["discount_price"].stringValue,"variant_name":data["default_variant_name"].stringValue,"original_price":data["original_price"].stringValue]
                    self.Trending_Products_Array.append(productObj)
                }
                
                self.Collectionview_TrendingList.delegate = self
                self.Collectionview_TrendingList.dataSource = self
                self.Collectionview_TrendingList.reloadData()
                //self.Height_TrendingCollectionview.constant = ((UIScreen.main.bounds.width - 54) / 2) + 60
              
                print("-------------------\(self.Trending_Products_Array.count)-------------------")
                
            }
            
            else if status == "9"
            {
                UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userId)
                UserDefaultManager.setStringToUserDefaults(value: "", key: UD_BearerToken)
                UserDefaultManager.setStringToUserDefaults(value: "", key: UD_TokenType)
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                nav.navigationBar.isHidden = true
                keyWindow?.rootViewController = nav
            }
            
            else
            {
                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: jsonResponse!["data"]["message"].stringValue.replacingOccurrences(of: "\\n", with: "\n"))
            }
        }
    }
}
